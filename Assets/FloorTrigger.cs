﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class FloorTrigger : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "PlayerTrigger")
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
