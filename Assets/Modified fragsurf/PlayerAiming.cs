﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerAiming : MonoBehaviour {

    [Header ("References")]
    public Transform bodyTransform = null;

    [Header("Sensitivity")]
    public float sensitivityMultiplier;
    public float horizontalSensitivity = 1f;
    public float verticalSensitivity = 1f;

    [Header ("Restrictions")]
    public float minYRotation = -90f;
    public float maxYRotation = 90f;
    
    // Rotation values
    [HideInInspector] public float bodyRotation = 0f;
    [HideInInspector] public Vector3 cameraRotation = Vector3.zero;

    private float bodyRotationTemp = 0f;
    private Vector3 cameraRotationTemp = Vector3.zero;

    // Leaning
    [HideInInspector] public float leanInput = 0f;

    // Sway
    [HideInInspector] public float sway = 0f;

    public TabMenu menu;
    public EndTrigger EndTrigger;

    void Start () {
        print("sensitivity:" + sensitivityMultiplier);
        // Lock the mouse
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        if (PlayerPrefs.GetFloat("MouseSensitivity") == 0)
            PlayerPrefs.SetFloat("MouseSensitivity", 1f);
        mouseSensitivityMultiplier = PlayerPrefs.GetFloat("MouseSensitivity");
        sensitivityMultiplier = PlayerPrefs.GetFloat("MouseSensitivity");
        mouseSensitivityMultiplier = sensitivityMultiplier;
        print("sensitivity:" + sensitivityMultiplier);

    }
    //UI Slider
    public void OnSliderValueChanged(float value)
    {
        sensitivityMultiplier = value;
        mouseSensitivityMultiplier = PlayerPrefs.GetFloat("MouseSensitivity");
        print("sensitivity:" + sensitivityMultiplier);
    }

    public float mouseSensitivityMultiplier;

    void Update () {
        mouseSensitivityMultiplier = PlayerPrefs.GetFloat("MouseSensitivity");

        if (sensitivityMultiplier != mouseSensitivityMultiplier)
            sensitivityMultiplier = mouseSensitivityMultiplier;

        if (menu.menuactive == true || EndTrigger.endGame == true)
        {
            sensitivityMultiplier = sensitivityMultiplier * 0.5f;
        }
        else if (menu.menuactive == false || EndTrigger.endGame == false)
        {
            sensitivityMultiplier = mouseSensitivityMultiplier;
        }
        
        Vector3 eulerAngles = transform.localEulerAngles;

        // Remove previous rotation
        eulerAngles = new Vector3 (eulerAngles.x - cameraRotationTemp.x, eulerAngles.y, eulerAngles.z - cameraRotationTemp.z);
        bodyTransform.eulerAngles -= cameraRotationTemp.y * Vector3.up;
        
        // Fix pausing
        if (Time.timeScale == 0f)
            return;

        // Input
        float xMovement = Input.GetAxis ("Mouse X") * horizontalSensitivity * sensitivityMultiplier;
        float yMovement = -Input.GetAxis ("Mouse Y") * verticalSensitivity * sensitivityMultiplier;
        
        // Rotate camera
        cameraRotation = new Vector3 (Mathf.Clamp (cameraRotation.x + yMovement, minYRotation, maxYRotation),
                                      cameraRotation.y + xMovement,
                                      cameraRotation.z + sway);

        cameraRotation.z = Mathf.Lerp (cameraRotation.z, 0f, Time.deltaTime * 3f);

        // Apply rotation
        Vector3 clampedRotation = new Vector3 (Mathf.Clamp (cameraRotation.x, minYRotation, maxYRotation),
                                               cameraRotation.y,
                                               cameraRotation.z);

        eulerAngles = new Vector3 (eulerAngles.x + clampedRotation.x, eulerAngles.y, eulerAngles.z + clampedRotation.z);
        bodyTransform.eulerAngles += Vector3.Scale (clampedRotation, new Vector3 (0f, 1f, 0f));
        cameraRotationTemp = clampedRotation;
        
        // Remove recoil
        transform.localEulerAngles = eulerAngles;

    }

}
