﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Fragsurf.Movement {

    /// <summary>
    /// Easily add a surfable character to the scene
    /// </summary>
    [AddComponentMenu ("Fragsurf/Surf Character")]
    public class SurfCharacter : MonoBehaviour, ISurfControllable {



        public enum ColliderType {
            Capsule,
            Box
        }

        ///// Fields /////

        [Header("Physics Settings")]
        public Vector3 colliderSize = new Vector3 (1f, 2f, 1f);
        [HideInInspector] public ColliderType collisionType { get { return ColliderType.Box; } } // Capsule doesn't work anymore; I'll have to figure out why some other time, sorry.
        public float weight = 75f;
        public float rigidbodyPushForce = 2f;
        public bool solidCollider = false;

        [Header("View Settings")]
        public Transform viewTransform;
        public Transform playerRotationTransform;

        [Header ("Crouching setup")]
        public float crouchingHeightMultiplier = 0.5f;
        public float crouchingSpeed = 10f;
        float defaultHeight;
        bool allowCrouch = true; // This is separate because you shouldn't be able to toggle crouching on and off during gameplay for various reasons

        [Header ("Features")]
        public bool crouchingEnabled = true;
        public bool slidingEnabled = false;

        [Header ("Step offset (can be buggy, enable at your own risk)")]
        public bool useStepOffset = false;
        public float stepOffset = 0.35f;

        [Header ("Movement Config")]
        [SerializeField]
        public MovementConfig movementConfig;
        
        private GameObject _groundObject;
        private Vector3 _baseVelocity;
        private Collider _collider;
        private Vector3 _angles;
        private Vector3 _startPosition;
        private GameObject _colliderObject;





        //  private GameObject _cameraWaterCheckObject;
        //  private CameraWaterCheck _cameraWaterCheck;

        private MoveData _moveData = new MoveData ();
        private SurfController _controller = new SurfController ();

        private Rigidbody rb;

        private List<Collider> triggers = new List<Collider> ();
        private int numberOfTriggers = 0;

      //  private bool underwater = false;

        ///// Properties /////

        public MoveType moveType { get { return MoveType.Walk; } }
        public MovementConfig moveConfig { get { return movementConfig; } }
        public MoveData moveData { get { return _moveData; } }
        public new Collider collider { get { return _collider; } }

        public GameObject groundObject {

            get { return _groundObject; }
            set { _groundObject = value; }

        }

        public Vector3 baseVelocity { get { return _baseVelocity; } }

        public Vector3 forward { get { return viewTransform.forward; } }
        public Vector3 right { get { return viewTransform.right; } }
        public Vector3 up { get { return viewTransform.up; } }

        Vector3 prevPosition;

        ///// Methods /////

        private void Awake () {
            _controller.playerTransform = playerRotationTransform;
            
            if (viewTransform != null) {

                _controller.camera = viewTransform;
                _controller.cameraYPos = viewTransform.localPosition.y;

            }

        }

        private void Start () {
            
            _colliderObject = new GameObject ("PlayerCollider");
            _colliderObject.layer = gameObject.layer;
            _colliderObject.transform.SetParent (transform);
            _colliderObject.transform.rotation = Quaternion.identity;
            _colliderObject.transform.localPosition = Vector3.zero;
            _colliderObject.transform.SetSiblingIndex (0);

            prevPosition = transform.position;

            if (viewTransform == null)
                viewTransform = Camera.main.transform;

            if (playerRotationTransform == null && transform.childCount > 0)
                playerRotationTransform = transform.GetChild (0);

            _collider = gameObject.GetComponent<Collider> ();

            if (_collider != null)
                GameObject.Destroy (_collider);

            // rigidbody is required to collide with triggers
            rb = gameObject.GetComponent<Rigidbody> ();
            if (rb == null)
                rb = gameObject.AddComponent<Rigidbody> ();

            allowCrouch = crouchingEnabled;

            rb.isKinematic = true;
            rb.useGravity = false;
            rb.angularDrag = 0f;
            rb.drag = 0f;
            rb.mass = weight;

            jumpSkillDuration = 0;

            switch (collisionType) {

                // Box collider
                case ColliderType.Box:

                _collider = _colliderObject.AddComponent<BoxCollider> ();

                var boxc = (BoxCollider)_collider;
                boxc.size = colliderSize;

                defaultHeight = boxc.size.y;

                break;

                // Capsule collider
                case ColliderType.Capsule:

                _collider = _colliderObject.AddComponent<CapsuleCollider> ();

                var capc = (CapsuleCollider)_collider;
                capc.height = colliderSize.y;
                capc.radius = colliderSize.x / 2f;

                defaultHeight = capc.height;

                break;

            }

            _moveData.slopeLimit = movementConfig.slopeLimit;

            _moveData.rigidbodyPushForce = rigidbodyPushForce;

            _moveData.slidingEnabled = slidingEnabled;

            _moveData.playerTransform = transform;
            _moveData.viewTransform = viewTransform;
            _moveData.viewTransformDefaultLocalPos = viewTransform.localPosition;

            _moveData.defaultHeight = defaultHeight;
            _moveData.crouchingHeight = crouchingHeightMultiplier;
            _moveData.crouchingSpeed = crouchingSpeed;
            
            _collider.isTrigger = !solidCollider;
            _moveData.origin = transform.position;
            _startPosition = transform.position;

            _moveData.useStepOffset = useStepOffset;
            _moveData.stepOffset = stepOffset;

        }

        //Timer starts when leaving Starting Area





        //Timer
        /*  public Text Timer;
          public bool timerReset = false; 
          private float seconds = 0.0f;
          private float minutes = 0.0f;*/

        public KeyCode ReloadScene;
        public KeyCode SkillUse;

        float t = 0;

        void LateUpdate()
        {
            //Speed
            /*    movementSpeedPrevious = transform.position;
                movementSpeedDistance = (movementSpeedPrevious - movementSpeedActual);
                movementSpeed = Mathf.RoundToInt(movementSpeedDistance.magnitude * 1f / Time.deltaTime);
           //     movementSpeed = Mathf.RoundToInt(movementSpeed);
                movementSpeedActual = transform.position;
                Speed.text = movementSpeed.ToString() + "km/h"; */
        }

        //Speed Variables
        float movementSpeed;
        float generalMovementSpeedTotal;
        float previousMovementSpeedTotal;
        float currentMovementSpeedTotal = 1f;
        float timeHighestSpeed = 0;
        public int playerSpeed;
        int n = 0;
        int d = 0;
        float timeMultiplier = 1;
        float distanceCount;
        public int highestSpeed = 0;

        float movementSpeedAverage;

        Vector3 movementSpeedDistance;
        Vector3 movementSpeedPrevious;
        Vector3 movementSpeedActual;
        public Text Speed;
        public Text Distance;
        public Text FinalDistance;

        public GameProgress Progress;
        bool jumpSkill = false;
        float jumpSkillDuration = 0;
        //public GameObject JumpSkillOverlay;

        private void Update () {

            //Press R to reset
            if (Input.GetKeyDown(ReloadScene))
            {
                if (Random.Range(0, 15) == 1)
                    CrazyAds.Instance.beginAdBreak();

                Scene scene = SceneManager.GetActiveScene();  SceneManager.LoadScene(scene.name);
            }

            _colliderObject.transform.rotation = Quaternion.identity;

            //UpdateTestBinds ();
            UpdateMoveData ();
            
            // Previous movement code
            Vector3 positionalMovement = transform.position - prevPosition;
            transform.position = prevPosition;
            moveData.origin += positionalMovement;

            //Speed
            t += Time.deltaTime;

            if (t >= 0.025f)
            {
                movementSpeedPrevious = transform.position;
                movementSpeedDistance = (movementSpeedPrevious - movementSpeedActual);
                movementSpeed = Mathf.RoundToInt(movementSpeedDistance.magnitude * 1f / Time.deltaTime);

               if (n <= Mathf.RoundToInt(12 / timeMultiplier)) //adding together a bunch of speeds
                {
                    generalMovementSpeedTotal = generalMovementSpeedTotal + movementSpeed;
                    n++;
                }
               if (n == Mathf.RoundToInt(6 / timeMultiplier)) //saving the previous speed
               {
                    previousMovementSpeedTotal = generalMovementSpeedTotal;
                    generalMovementSpeedTotal = 0;
               }
               if (n == Mathf.RoundToInt(12 / timeMultiplier)) //saving the current speed
                {
                    currentMovementSpeedTotal = generalMovementSpeedTotal;
                    generalMovementSpeedTotal = 0;
                    n = 0;
                }

                if (previousMovementSpeedTotal < currentMovementSpeedTotal) //subtracting the previous speed from the current speed until the previous speed is the same as current speed => giving out previous speed
                    previousMovementSpeedTotal += ((currentMovementSpeedTotal - previousMovementSpeedTotal) / (6 / timeMultiplier));  

                if (currentMovementSpeedTotal < 130) //The faster the player goes, the more rounded the numbers become
                    timeMultiplier = 3f;
                else if (currentMovementSpeedTotal > 130 && currentMovementSpeedTotal < 400)
                    timeMultiplier = 2f;
                else if (currentMovementSpeedTotal >= 400 && currentMovementSpeedTotal < 800)
                    timeMultiplier = 1f;
                else if (currentMovementSpeedTotal >= 800 && currentMovementSpeedTotal < 1200)
                    timeMultiplier = 0.5f;

                playerSpeed = Mathf.RoundToInt((previousMovementSpeedTotal) / (6 / timeMultiplier)); //Giving out the Speed
                Speed.text = playerSpeed.ToString() + "km/h";
                movementSpeedActual = transform.position;
                t = 0;

                //Distance
                if (d == 10)
                    distanceCount += movementSpeed / 100;
                
                if (d < 10)
                    d++;
                Distance.text = Mathf.RoundToInt(distanceCount) + "m";
                FinalDistance.text = Mathf.RoundToInt(distanceCount) + "m";
            }
            timeHighestSpeed += Time.deltaTime;
            if (Mathf.RoundToInt((previousMovementSpeedTotal) / (6 / timeMultiplier)) > highestSpeed && timeHighestSpeed >= 3)
            {
                highestSpeed = playerSpeed;
            }


            //Skill: Jump
            /*if (Input.GetKeyDown(SkillUse) && Progress.skillUsageTimes > 0)
            {
                jumpSkillDuration = 2;
                Progress.skillUsageTimes--;
                jumpSkill = true;
            }*/

            /*if (jumpSkill && jumpSkillDuration > 0)
            {
                moveConfig.jumpForce = 35;
                jumpSkillDuration = jumpSkillDuration - Time.deltaTime;
                JumpSkillOverlay.transform.localScale = new Vector3(1, jumpSkillDuration/2, 1);
            }

            if (jumpSkillDuration <= 0 && jumpSkill)
            {
                moveConfig.jumpForce = 8;
                jumpSkillDuration = 0;
                jumpSkill = false;
            }*/



         //   _moveData.cameraUnderwater = _cameraWaterCheck.IsUnderwater ();
         //   _cameraWaterCheckObject.transform.position = viewTransform.position;
         //   moveData.underwater = underwater;
            
            if (allowCrouch)
                _controller.Crouch (this, movementConfig, Time.deltaTime);

            _controller.ProcessMovement (this, movementConfig, Time.deltaTime);

            transform.position = moveData.origin;
            prevPosition = transform.position;

            _colliderObject.transform.rotation = Quaternion.identity;

         /*   if (Input.GetKey(moveForward))
                verticalEnable = 1f;
            if (Input.GetKey(moveBackward))
                verticalEnable = -1f;
            if (!Input.GetKey(moveForward) && !Input.GetKey(moveBackward))
                verticalEnable = 0f;

            if (Input.GetKey(moveLeft))
                horizontalEnable = -1f;
            if (Input.GetKey(moveRight))
                horizontalEnable = 1f;
            if (!Input.GetKey(moveLeft) && !Input.GetKey(moveRight))
                horizontalEnable = 0f; 

            if (Input.GetKey(Jump))
            {
                jumpEnable = true;
                UpdateMoveData();
            }
            if (!Input.GetKey(Jump))
                jumpEnable = false;
            UpdateMoveData(); */
        }
        
        private void UpdateTestBinds () {

            if (Input.GetKeyDown (KeyCode.Backspace))
                ResetPosition ();

        }

        private void ResetPosition () {
            
            moveData.velocity = Vector3.zero;
            moveData.origin = _startPosition;

        }

        //move Variables
        public KeyCode moveForward = KeyCode.W;
        public KeyCode moveBackward = KeyCode.S;
        public float verticalEnable = 0f;

        public KeyCode moveLeft = KeyCode.A;
        public KeyCode moveRight = KeyCode.D;
        public float horizontalEnable = 0f;

        public KeyCode Jump = KeyCode.Space;
        public bool jumpEnable = false;

        public void UpdateMoveData () {

            if (Input.GetKeyDown(KeyCode.F)) {
                _moveData.forwardMove = moveConfig.acceleration;
            }

            _moveData.verticalAxis = verticalEnable;//Input.GetAxisRaw ("Vertical");
            _moveData.horizontalAxis = horizontalEnable; //Input.GetAxisRaw ("Horizontal");

            _moveData.sprinting = Input.GetButton ("Sprint");
            
            if (Input.GetButtonDown ("Crouch"))
                _moveData.crouching = true;

            if (!Input.GetButton ("Crouch"))
                _moveData.crouching = false;
            
            bool moveLeft = _moveData.horizontalAxis < 0f;
            bool moveRight = _moveData.horizontalAxis > 0f;
            bool moveFwd = _moveData.verticalAxis > 0f;
            bool moveBack = _moveData.verticalAxis < 0f;
            bool jump = jumpEnable;//Input.GetButton ("Jump"); 

            if (!moveLeft && !moveRight)
                _moveData.sideMove = 0f;
            else if (moveLeft)
                _moveData.sideMove = -moveConfig.acceleration;
            else if (moveRight)
                _moveData.sideMove = moveConfig.acceleration;

            if (!moveFwd && !moveBack)
                _moveData.forwardMove = 0f;
            else if (moveFwd)
                _moveData.forwardMove = moveConfig.acceleration;
            else if (moveBack)
                _moveData.forwardMove = -moveConfig.acceleration; 
            
            if (jumpEnable == true)//Input.GetButtonDown ("Jump"))
                _moveData.wishJump = true;

            if (jumpEnable == false)//!Input.GetButton ("Jump"))
                _moveData.wishJump = false; 
            
            _moveData.viewAngles = _angles;

        }

        private void DisableInput () {

            _moveData.verticalAxis = 0f;
            _moveData.horizontalAxis = 0f;
            _moveData.sideMove = 0f;
            _moveData.forwardMove = 0f;
            _moveData.wishJump = false;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="angle"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public static float ClampAngle (float angle, float from, float to) {

            if (angle < 0f)
                angle = 360 + angle;

            if (angle > 180f)
                return Mathf.Max (angle, 360 + from);

            return Mathf.Min (angle, to);

        }

        /*private void OnTriggerEnter (Collider other) {
            
            if (!triggers.Contains (other))
                triggers.Add (other);

        }

        private void OnTriggerExit (Collider other) {
            
            if (triggers.Contains (other))
                triggers.Remove (other);

        }*/

        private void OnCollisionStay (Collision collision) {

            if (collision.rigidbody == null)
                return;

            Vector3 relativeVelocity = collision.relativeVelocity * collision.rigidbody.mass / 50f;
            Vector3 impactVelocity = new Vector3 (relativeVelocity.x * 0.0025f, relativeVelocity.y * 0.00025f, relativeVelocity.z * 0.0025f);

            float maxYVel = Mathf.Max (moveData.velocity.y, 10f);
            Vector3 newVelocity = new Vector3 (moveData.velocity.x + impactVelocity.x, Mathf.Clamp (moveData.velocity.y + Mathf.Clamp (impactVelocity.y, -0.5f, 0.5f), -maxYVel, maxYVel), moveData.velocity.z + impactVelocity.z);

            newVelocity = Vector3.ClampMagnitude (newVelocity, Mathf.Max (moveData.velocity.magnitude, 30f));
            moveData.velocity = newVelocity;

        }

    }

}

