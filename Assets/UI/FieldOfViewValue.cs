﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FieldOfViewValue : MonoBehaviour
{
    public Text Value;

    private void Start()
    {
        Value = GetComponent<Text>();

        fieldOfViewSlider.value = PlayerPrefs.GetInt("FieldOfView", 90);
        if (PlayerPrefs.GetInt("FieldOfView") > 170)
            fieldOfViewSlider.value = 90;
        else if(PlayerPrefs.GetInt("FieldOfView") < 10)
            fieldOfViewSlider.value = 90;
    }

    public Slider fieldOfViewSlider;

    public void OnSliderValueChanged(float value)
    {
        PlayerPrefs.SetInt("FieldOfView", Mathf.RoundToInt(fieldOfViewSlider.value));
        PlayerPrefs.Save();
        Value.text = value.ToString();
    }
}
