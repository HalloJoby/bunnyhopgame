﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoStreamScript : MonoBehaviour
{
    public string videoName;
    public VideoPlayer player;

    void Start()
    {
        player.url = System.IO.Path.Combine(Application.streamingAssetsPath, videoName);
        player.Play();
    }

}