﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyBinds : MonoBehaviour
{
    private Dictionary<string, KeyCode> keys = new Dictionary<string, KeyCode>();

    public Text forward, backward, left, right, jump, reload, skill;

    private GameObject currentKey;

    private Color32 normal = new Color32(0, 0, 0, 50);
    private Color32 selected = new Color32(133, 133, 133, 50);

    void Start()
    {
        keys.Add("Forward", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Forward","W")));
        keys.Add("Backward", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Backward", "S")));
        keys.Add("Left", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left", "A")));
        keys.Add("Right", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right", "D")));
        keys.Add("Jump", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Jump", "Space")));
        keys.Add("Skill", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Skill", "Q")));
        keys.Add("Reload", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Reload", "R")));


        forward.text = keys["Forward"].ToString();
        backward.text = keys["Backward"].ToString();
        left.text = keys["Left"].ToString();
        right.text = keys["Right"].ToString();
        jump.text = keys["Jump"].ToString();
        skill.text = keys["Skill"].ToString();
        reload.text = keys["Reload"].ToString();
    }


    public Fragsurf.Movement.SurfCharacter surfCharacter;

    void Update()
    {
        if (Input.GetKey(keys["Forward"]))
            surfCharacter.verticalEnable = 1f;
        if (Input.GetKey(keys["Backward"]))
            surfCharacter.verticalEnable = -1f;
        if (!Input.GetKey(keys["Forward"]) && !Input.GetKey(keys["Backward"]))
            surfCharacter.verticalEnable = 0f;

        if (Input.GetKey(keys["Left"]))
            surfCharacter.horizontalEnable = -1f;
        if (Input.GetKey(keys["Right"]))
            surfCharacter.horizontalEnable = 1f;
        if (!Input.GetKey(keys["Left"]) && !Input.GetKey(keys["Right"]))
            surfCharacter.horizontalEnable = 0f;

        if (Input.GetKey(keys["Jump"]))
        {
            surfCharacter.jumpEnable = true;
            surfCharacter.UpdateMoveData();
        }
        if (!Input.GetKey(keys["Jump"]))
            surfCharacter.jumpEnable = false;
            surfCharacter.UpdateMoveData();

        surfCharacter.SkillUse = keys["Skill"];

        surfCharacter.ReloadScene = keys["Reload"];
    }

    private void OnGUI()
    {
        if (currentKey != null)
        {
            Event e = Event.current;
            if (e.isKey)
            {
                keys[currentKey.name] = e.keyCode;
                currentKey.transform.GetChild(0).GetComponent<Text>().text = e.keyCode.ToString();
                currentKey.GetComponent<Image>().color = normal;
                currentKey = null;

                SaveKeys();
            }
        }
    }

    public void ChangeKey(GameObject clicked)
    {
        if (currentKey != null)
        {
            currentKey.GetComponent<Image>().color = normal;
        }

        currentKey = clicked;
        currentKey.GetComponent<Image>().color = selected;

    }

    public void SaveKeys()
    {
        foreach (var key in keys)
        {
            PlayerPrefs.SetString(key.Key, key.Value.ToString());
        }
        PlayerPrefs.Save();
    }
}