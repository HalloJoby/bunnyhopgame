﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandPlayAttack : MonoBehaviour
{
    [SerializeField] private Texture2D[] frames;
    //[SerializeField] private float fps = 30.0f;

    private Material handMat;
    public int index = 0;

    void Start()
    {
        handMat = GetComponent<Renderer>().material;
    }

    public float indexCount = 0;

    void Update()
    {
        indexCount += Time.deltaTime;
        if (indexCount >= 0.07f)
        {
            if (index == frames.Length - 1)
                index = 0;

            index++;
            handMat.mainTexture = frames[index];
            indexCount = 0;
        }

        print("index is:" + index);

        //index = Mathf.RoundToInt((Time.deltaTime * fps));
        //index = index % frames.Length;
    }
}
