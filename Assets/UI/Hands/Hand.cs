﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hand : MonoBehaviour
{
    public GameObject Idle;
    public GameObject Attack;
    public GameObject Inspect;
    public HandPlay HandPlayIdle;
    public HandPlayAttack HandPlayAttack;
    public HandPlay HandPlayInspect;

    void Start()
    {
        Attack.SetActive(false);
        Inspect.SetActive(false);
    }

    public bool idleActive = true;
    public bool attackActive = false;
    public bool inspectActive = false;


    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Attack.SetActive(true);
            attackActive = true;
            inspectActive = false;
            HandPlayAttack.index = 0;
        }

        if (Input.GetMouseButtonDown(1))
        {
            Inspect.SetActive(true);
            inspectActive = true;
            attackActive = false;
            HandPlayInspect.index = 0;

            bool Boolean = (Random.value > 0.5f);
            if (Boolean)
            {
                HandPlayInspect.index = 24;
            }
        }

        if (attackActive)
        {
            Idle.SetActive(false);
            Attack.SetActive(true);
            Inspect.SetActive(false);
            if (HandPlayAttack.index == 6)
            {
                attackActive = false;
                Idle.SetActive(true);
                Attack.SetActive(false);
                Inspect.SetActive(false);
            }
        }

        if (inspectActive)
        {
            Idle.SetActive(false);
            Attack.SetActive(false);
            Inspect.SetActive(true);
            if (HandPlayInspect.index == 35)
            {
                inspectActive = false;
                Idle.SetActive(true);
                Attack.SetActive(false);
                Inspect.SetActive(false);
            }
        }
    }

}
