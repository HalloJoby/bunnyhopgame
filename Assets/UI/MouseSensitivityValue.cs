﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseSensitivityValue : MonoBehaviour
{
    public Text Value;
    public PlayerAiming PlayerAiming;

    private void Start()
    {
        Value = GetComponent<Text>();
        mouseSensitivitySlider.value = PlayerPrefs.GetFloat("MouseSensitivity", 1);
        if (PlayerPrefs.GetFloat("MouseSensitivity") == 0)
            mouseSensitivitySlider.value = 1f;
        else if (PlayerPrefs.GetFloat("MouseSensitivity") > 5f)
            mouseSensitivitySlider.value = 1f;
        else if (PlayerPrefs.GetFloat("MouseSensitivity") < 0.01f)
            mouseSensitivitySlider.value = 1f;
    }

    public Slider mouseSensitivitySlider;

    public void OnSliderValueChanged(float value)
    {
        PlayerPrefs.SetFloat("MouseSensitivity", mouseSensitivitySlider.value);
        //PlayerAiming.mouseSensitivityMultiplier = PlayerPrefs.GetFloat("MouseSensitivity");
        PlayerPrefs.Save();
        Value.text = value.ToString();
    }
}
