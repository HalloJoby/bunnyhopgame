﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TabMenu : MonoBehaviour
{
    public bool menuactive = false;
    public EndTrigger EndTrigger;
    public GameProgress GameProgress;

    public GameObject LevelTutorialBronze;
    public GameObject LevelTutorialSilver;
    public GameObject LevelTutorialGold;

    public GameObject Level1Bronze;
    public GameObject Level1Silver;
    public GameObject Level1Gold;

    public GameObject Level2Bronze;
    public GameObject Level2Silver;
    public GameObject Level2Gold;

    public GameObject Level3Bronze;
    public GameObject Level3Silver;
    public GameObject Level3Gold;

    public GameObject Level4Bronze;
    public GameObject Level4Silver;
    public GameObject Level4Gold;

    /* 
    TUTORIAL TEXT MOVE:
    TO BUNNYHOP: HOLD JUMP BUTTON
    YOU CAN STEER MIDAIR BY HOLDING
    LEFT OR RIGHT AND MOVING YOUR MOUSE

    TUTORIAL TEXT SURF:
    TO SURF:
    HOLD LEFT/RIGHT OPPOSING THE SIDE
    OF THE TRIANGLE
    */


    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Tab) && EndTrigger.endGame == false)
        {
            menuactive = !menuactive;
        }

        if (menuactive == true)
        {
            GUI.enabled = true;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            Time.timeScale = 0.000001f;
            GetComponent<CanvasGroup>().alpha = 1;
        }

        if (menuactive == false && EndTrigger.endGame == false)
        {
            GUI.enabled = false;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            Time.timeScale = 1;
            GetComponent<CanvasGroup>().alpha = 0;
        }

        //Medals
        if (GameProgress.LevelTutorialMedal == 0)
        {
            LevelTutorialBronze.SetActive(false);
            LevelTutorialSilver.SetActive(false);
            LevelTutorialGold.SetActive(false);
        }
        else if (GameProgress.LevelTutorialMedal == 1)
        {
            LevelTutorialBronze.SetActive(true);
            LevelTutorialSilver.SetActive(false);
            LevelTutorialGold.SetActive(false);
        }
        else if(GameProgress.LevelTutorialMedal == 2)
        {
            LevelTutorialBronze.SetActive(false);
            LevelTutorialSilver.SetActive(true);
            LevelTutorialGold.SetActive(false);
        }
        else if(GameProgress.LevelTutorialMedal == 3)
        {
            LevelTutorialBronze.SetActive(false);
            LevelTutorialSilver.SetActive(false);
            LevelTutorialGold.SetActive(true);
        }
    }
}
