﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovementTutorial : MonoBehaviour
{
    public GameObject MoveTutorialScreen;
    bool active;

    private void OnTriggerStay(Collider other)
    {
        active = true;
    }
    private void OnTriggerExit(Collider other)
    {
        active = false;
    }

    private void Update()
    {
            MoveTutorialScreen.SetActive(active);
    }
}
