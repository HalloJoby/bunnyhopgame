﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SurfTutorial : MonoBehaviour
{
    public GameObject SurfTutorialScreen;
    bool active;
    public string sceneName;

    public void Start()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        sceneName = currentScene.ToString();
    }

    private void OnTriggerStay(Collider other)
    {
        active = true;
    }

    private void OnTriggerExit(Collider other)
    {
        active = false;
    }

    private void Update()
    {
            SurfTutorialScreen.SetActive(active);
    }
}
