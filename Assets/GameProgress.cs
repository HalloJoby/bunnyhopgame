﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameProgress : MonoBehaviour
{
    public StartTrigger StartTrigger;
    public EndTrigger EndTrigger;

    public int LevelTutorialMedal;
    public int Level1Medal;
    public int Level2Medal;
    public int Level3Medal;
    public int Level4Medal;

    private Scene scene;

    //public Text SkillUsage;
    //public int skillUsageTimes = 0;

    private void Start()
    {
        //skillUsageTimes = 0;
        scene = SceneManager.GetActiveScene();
    }

    private void Awake()
    {
        LevelTutorialMedal = PlayerPrefs.GetInt("TutorialMedal");
    }

    private void Update()
    {
        if (LevelTutorialMedal == 1)

        

        if (scene.name == "LevelTutorial" && StartTrigger.setBronze && EndTrigger.LevelTutorialEnd && LevelTutorialMedal != 2 && LevelTutorialMedal != 3)
        {
            LevelTutorialMedal = 1;
            PlayerPrefs.SetInt("TutorialMedal", LevelTutorialMedal);
        }
        if (scene.name == "LevelTutorial" && StartTrigger.setSilver && EndTrigger.LevelTutorialEnd && LevelTutorialMedal != 3)
        {
            LevelTutorialMedal = 2;
            PlayerPrefs.SetInt("TutorialMedal", LevelTutorialMedal);
        }
        if (scene.name == "LevelTutorial" && StartTrigger.setGold && EndTrigger.LevelTutorialEnd)
        {
            LevelTutorialMedal = 3;
            PlayerPrefs.SetInt("TutorialMedal", LevelTutorialMedal);
        }

        //SkillUsage.text = new string('o', skillUsageTimes);
    }


    public bool LevelTutorialUnlock = true;
    public bool Level1Unlock = false;
    public bool Level2Unlock = false;
    public bool Level3Unlock = false;
    public bool Level4Unlock = false;

    public void LoadLevel(string levelName)
    {
            SceneManager.LoadScene(levelName);
    }


}
