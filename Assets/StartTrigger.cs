﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartTrigger : MonoBehaviour
{
    public Text Timer;
    public Text EndTriggerTimer;
    public Text HighestSpeed;
    public GameObject GoldMedal;
    public GameObject SilverMedal;
    public GameObject BronzeMedal;
    public Fragsurf.Movement.SurfCharacter SurfCharacter;
    public bool timerActive = false;
    private float seconds = 0.0f;
    private float minutes = 0.0f;

    public Text RequiredTimeBronze;
    public Text RequiredTimeSilver;
    public Text RequiredTimeGold;
    public int requiredTimeGold;
    public int requiredTimeSilver;
    public int requiredTimeBronze;

    private void Start()
    {
        Scene scene = SceneManager.GetActiveScene();
        if (scene.name == "LevelTutorial")
        {
            requiredTimeBronze = 35;
            requiredTimeSilver = 25;
            requiredTimeGold = 22;
        }

        if (scene.name == "Level1")
        {
            requiredTimeBronze = 90;
            requiredTimeSilver = 73;
            requiredTimeGold = 65;
        }

        if (scene.name == "Level2")
        {
            requiredTimeBronze = 120;
            requiredTimeSilver = 100;
            requiredTimeGold = 90;
        }

        RequiredTimeBronze.text = Mathf.FloorToInt((requiredTimeBronze) / 60).ToString().PadLeft(1, '0') + ":" + (requiredTimeBronze % 60).ToString("F2").Replace(',', ':').PadLeft(5, '0');
        RequiredTimeSilver.text = Mathf.FloorToInt((requiredTimeSilver) / 60).ToString().PadLeft(1, '0') + ":" + (requiredTimeSilver % 60).ToString("F2").Replace(',', ':').PadLeft(5, '0');
        RequiredTimeGold.text = Mathf.FloorToInt((requiredTimeGold) / 60).ToString().PadLeft(1, '0') + ":" + (requiredTimeGold % 60).ToString("F2").Replace(',', ':').PadLeft(5, '0');
        GoldMedal.SetActive(false);
        SilverMedal.SetActive(false);
        BronzeMedal.SetActive(false);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name == "PlayerTrigger")
        {
            timerActive = true;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "PlayerTrigger")
        {
            timerActive = false;
        }
    }

    public bool setBronze;
    public bool setSilver;
    public bool setGold;

    void Update()
    {
        if (timerActive == false)
        {
            seconds = 0;
            minutes = 0;
        }
        else if (timerActive == true)
        {
            seconds += Time.deltaTime;
        }

        if (seconds > 60)
        {
            minutes++;
            seconds = 0;
        }
        Timer.text = minutes.ToString().PadLeft(1, '0') + ":" + seconds.ToString("F2").Replace(',', ':').PadLeft(5, '0');
        EndTriggerTimer.text = minutes.ToString().PadLeft(1, '0') + ":" + seconds.ToString("F2").Replace(',', ':').PadLeft(5, '0');

        //Medals
        if (seconds + (minutes * 60) < requiredTimeBronze && seconds + (minutes * 60) > requiredTimeSilver && seconds + (minutes * 60) > requiredTimeGold)
        {
            BronzeMedal.SetActive(true);
            setBronze = true;
        }
        else
        {
            BronzeMedal.SetActive(false);
            setBronze = false;
        }

        if (seconds + (minutes * 60) < requiredTimeSilver && seconds + (minutes * 60) > requiredTimeGold)
        {
            SilverMedal.SetActive(true);
            setSilver = true;
        }
        else
        {
            SilverMedal.SetActive(false);
            setSilver = false;
        }

        if (seconds + (minutes * 60) < requiredTimeGold)
        {
            GoldMedal.SetActive(true);
            setGold = true;
        }
        else
        {
            GoldMedal.SetActive(false);
            setGold = false;
        }

        HighestSpeed.text = SurfCharacter.highestSpeed.ToString() + "km/h";
    }
}
