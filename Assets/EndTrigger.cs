﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndTrigger : MonoBehaviour
{

    public bool endGame = false;
    public GameObject endGameUI;
    public TabMenu TabMenu;
    public StartTrigger StartTrigger;
    public GameProgress GameProgress;

    public bool LevelTutorialEnd;
    public bool Level1End;
    public bool Level2End;
    public bool Level3End;
    public bool Level4End;

    private void Start()
    {
        endGameUI.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        Scene scene = SceneManager.GetActiveScene();
        if (scene.name == "LevelTutorial")
        {
            LevelTutorialEnd = true;
            GameProgress.Level1Unlock = true;
        }
        if (scene.name == "Level1")
        {
            Level1End = true;
            GameProgress.Level2Unlock = true;
        }
        if (scene.name == "Level2")
        {
            Level2End = true;
            GameProgress.Level3Unlock = true;
        }
        if (scene.name == "Level3")
        {
            Level3End = true;
            GameProgress.Level4Unlock = true;
        }
        if (scene.name == "Level4")
        {
            Level4End = true;
        }
        endGame = true;
    }

    private void Update()
    {
        if (endGame)
        {

            endGameUI.SetActive(true);
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            Time.timeScale = 0.000001f;
        }
    }
}
