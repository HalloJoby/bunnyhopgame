﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillJump : MonoBehaviour
{
    public GameProgress Progress;

    private void Start()
    {
        transform.Rotate(0, 0, Random.Range(0f, 10f));
    }
    void OnTriggerEnter()
    {
        //Progress.skillUsageTimes++;
        Destroy(this.gameObject);
    }
    private void Update()
    {
        transform.Rotate(0, 0, -5f);
    }
}
