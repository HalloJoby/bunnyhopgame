﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VillageTrigger : MonoBehaviour
{

    public GameObject Village;
    public int asdf = 0;

    private void Start()
    {
        Village.SetActive(false);
    }

    public void OnTriggerEnter(Collider other)
    {
        Village.SetActive(true);
    }
}
